// Este cliente se implementa con ws

const Websocket = require('ws')

const client = new Websocket('ws://localhost:2020/mainpage','soap')

client.on('open',()=>{
    client.send('Hola Jsons')
    client.on('message',(msg)=>{
        console.log(msg)
    })
})

client.on('close',(code, reason)=>{
    console.log('codigo: ',code,' Razon: ',reason)
})

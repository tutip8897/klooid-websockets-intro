// Este cliente se implementa con socket.io-client

const io = require('socket.io-client')

const socket = io('ws://localhost:2040')

socket.on('connect',()=>{
    const start = Date.now()
    socket.emit('message')
    socket.on('recibido', ()=>{
        const millis = Date.now() - start
        console.log('Tomo: ',millis, 'ms en hacer la prueba de retorno')
    })
})


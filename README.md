### Klooid Websockets Intro
## Check out the video
[Youtube introductory video](https://www.youtube.com/watch?v=Rnqh9WIYJjQ)
## How to use
1. Run npm install
2. Open a new terminal with a server (ioserver or wsserver)
3. Open a new terminal with a client (ioclient or wsserver)
4. Modify and enjoy

# Note
ws is not compatible with socket.io. Attempting to mix them will not work.

Have fun :smile:

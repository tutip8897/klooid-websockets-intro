// Este servidor utiliza socketio

const port = 2040

const app = require('express')()

const server = require('http').createServer(app)

const io = require('socket.io')(server)

io.on('connection', (socket) =>{
    socket.on('message',()=>{
        socket.emit('recibido')
    })
    socket.on('disconnect', ()=>{
        console.log('Cliente se desconeto')
    })
})


server.listen(port, () =>{
    console.log('Socket.io running on port ', port)
})

// Este cliente se implementa con ws

const Websocket = require('ws')

const client = new Websocket('ws://localhost:2020/mainpage','json')

client.on('open',()=>{
    const time = Date.now()
    client.send('Hola soaps')
    client.on('message',(msg)=>{
        const millis = Date.now() - time
        console.log('La prueba de retorno tomo ', millis, 'ms')
    })
})

client.on('close',(code, reason)=>{
    console.log('codigo: ',code,' Razon: ',reason)
})

// Esta implementacion utiliza ws

const Websocket = require('ws')

const port  = 2020

let jsonsockets = []

let soapsockets = []

const server = new Websocket.Server({
    port:port
})

server.on('headers', (Headers) =>{
    console.log(Headers)
})

server.on('connection', (socket,req) =>{
    console.log(req.headers)
    if(req.headers['sec-websocket-protocol'] == 'json'){
        jsonsockets.push(socket)
        socket.send('Hola Json')
    }
    if(req.headers['sec-websocket-protocol'] == 'soap'){
        soapsockets.push(socket)
        socket.send('Hola Soap')
    }

    jsonsockets.forEach(s=>{
        s.on('message', (msg) =>{
            soapsockets.forEach(s=>{
                s.send(msg)
            })
        })
    })

    soapsockets.forEach(s=>{
        s.on('message', (msg) =>{
            jsonsockets.forEach(s=>{
                s.send(msg)
            })
        })
    })

})
